package com.tina;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TinaVideoApplication {

    public static void main(String[] args) {
        SpringApplication.run(TinaVideoApplication.class, args);
    }

}
