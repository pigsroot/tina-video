package com.tina.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @Author Tina
 * @Time 2020/9/17 10:42
 * @Version 1.0
 * @Content
 **/
@Controller
public class HomeController {

    @GetMapping({"/", "home"})
    public String home() {

        return "home";
    }

    @GetMapping("index")
    public String index() {

        return "index";
    }
}
